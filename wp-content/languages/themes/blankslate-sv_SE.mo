��          �   %   �      p     q  $   �     �     �     �  
   �     �     �     �     �  	          	   0     :  ;   H     �     �     �  5   �     �  
     	     
        !     5  (   L     u  	  ~     �  0   �  	   �     �     �  
   �     �                  	   )     3     D     P  K   c     �     �     �  4   �     &  
   4  	   ?  
   I     T     b  (   y     �                                                                                                 	   
                        Theme By: %1$s. %1$s %2$s %3$s. All Rights Reserved. %s older Archives Author Archives BlankSlate Categories:  Category Archives:  Comments Daily Archives: %s Main Menu Monthly Archives: %s Not Found Nothing Found Nothing found for the requested page. Try a search instead? Return to %s Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Tag Archives:  TidyThemes Trackback Trackbacks Yearly Archives: %s http://tidythemes.com/ https://github.com/tidythemes/blankslate newer %s PO-Revision-Date: 2018-07-19 12:19:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: sv_SE
Project-Id-Version: Themes - BlankSlate
 Tema av: %1$s. %1$s %2$s %3$s. Alla rättigheter förbehållna. %s äldre Arkiv Författar Arkiv BlankSlate Kategorier: Kategoriarkiv: Kommentarer Dagsarkiv: %s Huvudmeny månadsarkiv: %s Inte hittad Inget kunde hittas Ingenting hittades för den begärda sidan. Försök en sökning istället? Tillbaka till %s Sökresultat för: %s Widget-område i sidopanel Tyvärr, inget matchade din sökning. Försök igen. Etikettarkiv: TidyThemes Trackback Trackbacks Årsarkiv: %s http://tidythemes.com/ https://github.com/tidythemes/blankslate nyare %s 