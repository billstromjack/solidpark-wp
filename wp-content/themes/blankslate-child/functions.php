<?php
add_action( 'wp_enqueue_scripts', 'parent_style' );
function parent_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
?>