<?php
/**
 * Template Name: Mallen för Kunskapsbanken-sidan
 *
**/
get_header(); ?>

<!-- WP Content -->
<div id="entry" class="content page">
        <div class="page-template">
            <h1><?php the_title(); ?></h1>

            <div class="row">
                <p class="preamble">
                    <?php the_field('underrubrik'); ?>
                </p>
            </div>
        </div>
    </div>
<!--# WP Content -->

<div class="card-grid">
    <?php if(get_field('inlaggskort')): ?>
        <?php while(has_sub_field('inlaggskort')): ?>
        <a href="<?php the_sub_field('lank'); ?>" class="card zoom">
            <div class="card-content" style="background-image: url('<?php the_sub_field('bild'); ?>')">
                <p><?php the_sub_field('rubrik'); ?></p>
                <p><?php the_sub_field('underrubrik'); ?></p>

                <span  class="btn-primary">Läs mer »</span>
            </div>
        </a>
        <?php endwhile; ?>
    <!-- //Loop -->
    <?php endif; ?>
</div>
<?php get_footer(); ?>

<!--   Mall för kundreferens -->