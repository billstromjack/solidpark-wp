<?php 
    get_header(); 
?>

<div id="entry" class="content page">
    <div class="page-template">

    <h2><?php printf( __( 'Sökresultat för: %s', 'blankslate' ), get_search_query() ); ?></h2>

    <?php if ( have_posts() ) : ?>
        <div class="posts loop">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'entry' ); ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<!--# WP Content -->

    <?php else : ?>
        <h2><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
        <p><?php _e( 'Det gick inte att hitta någon med denna term, försök en annan.', 'blankslate' ); ?></p>
        </div>
        </div>
<?php endif; ?>

<?php get_footer(); ?>