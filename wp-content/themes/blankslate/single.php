<?php 
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
$thumb_url = $thumb_url_array[0];

function show_publish() {
    $categories = get_the_category();
    if (in_array("nyheter", array_column($categories, 'slug'))) { // search value in the array
        return false;
    }

    return true;
}

get_header(); ?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="entry" class="content page blog_article" style="max-width:1280px">
    <div class="page-template">
        <?php echo do_shortcode("[breadcrumb]"); ?>
        <h1><?php the_title(); ?></h1>

        <div class="row">

            <div class="col">

                <?php if (show_publish()): ?>
                    <div class="publish">
                        <a class="" href=""><?php echo get_the_author_meta('display_name', $author_id); ?></a>, Publicerat den <?php the_date(); ?>
                    </div>
                <?php endif; ?>

                <?php if (!show_publish()): ?>
                    <div class="publish">
                        Publicerat den <?php the_date(); ?>
                    </div>
                <?php endif; ?>

                <p class="preamble">

                    <?php the_field('ingress'); ?>

                </p>

                <!-- Utvald bild-->
                <div class="featured_img-container">
                    <img src="<?php echo $thumb_url;  ?>" alt="<?php echo $thumb_alt; ?>">
                </div>
                <!-- -// Utvald bild-->

                <?php the_content(); ?>

                <!-- Bio-->
                <?php if (show_publish()): ?>
                <div class="author-box">

                     <?php echo get_avatar( get_the_author_meta( 'ID' ), 100, $author_id ); ?>

                    <div class="author-detail">
                        <h4><?php echo get_the_author_meta('display_name', $author_id); ?></h4>
                        <p><?php echo get_the_author_meta('description', $author_id); ?></p>
                        <div>
                            <a href="<?php echo get_the_author_meta('url', $author_id); ?>"><img src="http://sp.ampilioutveckling.se/wp-content/themes/astra/assets/images/linkedin.png"
                                    alt="LinkedIn Profil"></a>
                            <a href="mailto:<?php echo get_the_author_meta('email', $author_id); ?>">
                                <i class="far fa-envelope"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- // Bio-->
                <?php endif; ?>
 
            </div>

            <div class="col">

                <?php if (show_publish()): ?>
                <div class="page-card">
                    <h2>Taggar</h2>

                    <div class="tag-cloud">
                        <?php foreach (get_the_tags(get_the_ID()) as $tag) {
                            ?> 
                                <span><a href="<?php echo get_tag_link($tag); ?>"><?php echo $tag->name; ?></a></span> 
                            <?php
                        } ?>
                    </div>
                </div>

                <div class="page-card">
                    <h2>Övriga inlägg</h2>
                    <?php $catquery = new WP_Query( 'cat=9&posts_per_page=10' ); ?>
                        <ul>
                        <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                        <li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></li>
                        <?php endwhile;
                            wp_reset_postdata();
                    ?>
                </div>
    
                    <?php else: ?>                
                        <div class="page-card">
                        <h2>Senaste inlägg</h2>
                        <?php $catquery = new WP_Query( 'cat=3&posts_per_page=10' ); ?>
                            <ul>
                            <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                            <li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></li>
                            <?php endwhile;
                                wp_reset_postdata();
                        ?>
                        </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>