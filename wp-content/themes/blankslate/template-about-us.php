<?php
/**
 * Template Name: Mall för Om oss
 *
**/
get_header(); ?>


<div class="hero">
    <div class="hero-inner">
        <div class="centered row">
            <h2>Solid Park levererar IT-drift samt systemutveckling, digitalisering och systemförvaltning.</h2>
        </div>
    </div>
</div>
<!-- # Plain hero -->

<!-- WP Content -->
<div class="row about">
    <div class="col">
        <div class="content">
            <div class="entry-content">
                <h2>
                    <?php the_title(); ?>
                </h2>

                <p>
                    <?php echo get_post_field('post_content', $post->ID); ?>
                </p>
            </div>
        </div>

        <!-- Sponsor row -->
        <div class="row sponsor">
            <?php 
                $images = get_field('skrytbilder');

                if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                        <div>
                        <img src="<?php echo $image['url']; ?>" alt="">
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
        </div>
        <!-- # Sponsor row-->
    </div>

    <div class="col">

        <?php if(get_field('kort_i_sidomeny')): ?>
        <?php while(has_sub_field('kort_i_sidomeny')): ?>
        <div class="card-grid">
            <a href="#" class="card zoom">
                <div class="card-content" style="background-image: url('<?php the_sub_field('bild'); ?>')">
                    <p>
                        <?php the_sub_field('rubrik'); ?>
                    </p>
                    <p></p>
                </div>
            </a>
        </div>
        <?php endwhile; ?>
        <!-- //Loop -->
    </div>
    <?php endif; ?>
</div>
</div>

<!--# WP Content -->
<div class="content">
    <div class="entry-content">
        <h2>
            <?php the_field('sekundar_rubrik'); ?>
        </h2>

        <p><?php the_field('sekundart_innehall'); ?></p>
    </div>
</div>

<!-- Kontaktformulär -->
<div class="full-width big" style="background: 
        linear-gradient(
          rgba(15,21,25,0.8), 
          rgba(15,21,25,0.8)
        ),
        url(http://sp.ampilioutveckling.se/wp-content/uploads/2018/06/om2.jpg); background-repeat:repeat, no-repeat;
        background-size: cover">
    <div class="col centered wide">
        <h2 class="one">Arbeta på Solid Park</h2>
        <h3 class="txt-left">Solid Park erbjuder spännande och utvecklande karriär i ett IT-företag som ligger
            absolut i framkant av både teknik och service.</h3>
        <h3 class="txt-left">Är du väldigt erfaren inom IT? Eller vill du bli det ? Här hittar aktuella lediga
            tjänster och kan även lämna in en spontanansökan.</h3>

        <a class="btn large bottom left" href="/jobba-hos-oss/">Lediga tjänster &gt;&gt;</a>
    </div>
</div>
<!-- # Kontaktformulär -->


<?php get_footer(); ?>