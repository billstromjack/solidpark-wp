<?php 
    get_header(); 
?>

<div id="entry" class="content page">
    <div class="page-template">

    <?php if ( have_posts() ) : ?>
        <div class="posts loop">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'entry' ); ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<!--# WP Content -->

    <?php else : ?>
        <h2 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
        <section class="entry-content">
        <p><?php _e( 'Det gick inte att hitta någon med denna term, försök en annan.', 'blankslate' ); ?></p>
    <?php get_search_form(); ?>
<?php endif; ?>

<?php get_footer(); ?>