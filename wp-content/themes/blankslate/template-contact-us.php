<?php
/**
 * Template Name: Mall för Kontakta Oss
 *
**/
get_header(); ?>

<div id="entry" class="content page blog_article">
    <div class="page-template">
        <div class="row">
            <div class="col contact-col">
                <h1><?php the_field('rubrik') ?></h1>
                <p><?php the_field('innehall') ?></p>
            </div>
            <div class="col contact-col">
                <h1><?php the_field('rubrik_hoger') ?></h1>
                <p><?php the_field('innehall_hoger') ?></p>
            </div>
        </div>
    </div>
</div>


<div class="card-grid">
    <?php if(get_field('stader')): ?>
        <?php while(has_sub_field('stader')): ?>
        <a href="<?php the_sub_field('lank'); ?>" class="card zoom">
            <div class="card-content" style="background-image: url('<?php the_sub_field('bild'); ?>')">
                <p><?php the_sub_field('ort'); ?></p>
                <p><?php the_sub_field('address'); ?></p>

                <span  class="btn-primary">Läs mer »</span>
            </div>
        </a>
        <?php endwhile; ?>
    <!-- //Loop -->
    <?php endif; ?>
</div>

<?php get_footer(); ?>