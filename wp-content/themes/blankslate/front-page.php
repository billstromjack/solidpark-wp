<?php
/**
 * Template Name: Startsidan
 *
**/ 
$id = 171;
get_header(); ?>

<!-- Video bg -->
<div class="full-video">

<video autoplay muted loop id="myVideo">
    <source src="http://sp.ampilioutveckling.se/wp-content/uploads/2018/10/Hemsida-3_1-1.mp4" type="video/mp4">
</video>

<div class="centered-content">
    <h1><?php the_field('rubrik', $id); ?></h1>
    <p><?php the_field('underrubrik', $id); ?></p>

    <div class="row">
        <?php 
            if ( have_rows('lankar', $id) ) :
                while ( have_rows('lankar', $id) ) : the_row(); ?>
                    <a href="<?php the_sub_field('lank_url'); ?>" class="btn-primary">
                        <?php the_sub_field('lanktext'); ?>
                    </a> 
                <?php endwhile;
            endif; 
        ?>
    </div>
</div>

</div>
<!-- # Video bg -->

<!-- Snyggrutorna -->
<div class="card-grid">

    <?php 
        if ( have_rows('puffar', $id) ) :
            while ( have_rows('puffar', $id) ) : the_row(); ?>
                <a href="<?php the_sub_field('lank'); ?>" class="card zoom">
                    <div class="card-content" style="background-image: url('<?php the_sub_field('bild'); ?>">                    
                        <p><?php the_sub_field('rubrik'); ?></p>
                        <p><?php the_sub_field('underrubrik'); ?></p>
                        <span class="btn-primary">Läs mer »</span>
                    </div>
                </a> 
            <?php endwhile;
        endif; 
    ?>

</div>
<!-- # Snyggrutorna -->

<!-- Kontaktformulär -->
<div class="full-width" style="background: 
linear-gradient(
  rgba(15,21,25,0.8), 
  rgba(15,21,25,0.8)
),
url(http://sp.ampilioutveckling.se/wp-content/uploads/2018/04/solidpark.jpeg); background-repeat:repeat, no-repeat;
background-size: cover">
<div class="col">
    <h2>Prata med en specialist</h2>
    <p>Är du befintlig kund så hittar du ett telefonnummer till Service Desk och driftinformation lite längre
        ner på den här sidan.</p>
</div>

<div class="col">

    <form action="">
        <div class="row">
            <input type="text" placeholder="Förnamn & efternamn" />
        </div>

        <div class="row">
            <input type="text" class="half" placeholder="E-post">
            <input type="text" class="half" placeholder="Telefonnummer">
        </div>

        <div class="row">
            <textarea name="" id="" cols="30" rows="5" placeholder="Vad vill du prata om?"></textarea>
        </div>

        <div style="margin: 5px 0px">
        </div>

        <button class="btn-primary">Kontakta mig!</button>
    </form>
</div>
</div>
<!-- # Kontaktformulär -->




<?php get_footer(); ?>