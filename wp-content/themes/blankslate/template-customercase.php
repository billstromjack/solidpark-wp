<?php
/**
 * Template Name: Mall för kundreferens
 *
**/

$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];

get_header(); ?>

<div id="entry" class="content page" style="max-width: 1280px">
        <div class="page-template">
            <?php echo do_shortcode("[breadcrumb]"); ?>
            <h1><?php the_title(); ?></h1>

            <div class="row">

                <div class="col">
                    <p class="preamble">

                        <?php the_field('ingress'); ?>

                    </p>

                    <?php the_field('innehall'); ?>

                </div>

                <div class="col">

                    <div class="featured_img-container">
                        <img src="<?php echo $thumb_url ?>"
                            alt="">
                    </div>

                    <?php if(get_field('hogerstallt_kort')): ?>
                        <?php while(has_sub_field('hogerstallt_kort')): ?>
                        <div class="page-card">
                                <h2><?php the_sub_field('rubrik'); ?></h2>
                                <?php the_sub_field('innehall'); ?>
                        </div>
                        <?php endwhile; ?>
                    <!-- //Loop -->
                    <?php endif; ?>


                </div>
            </div>
        </div>
    </div>
    <!--# WP Content -->

    <div class="section centered branded">
        <h3 style="font-weight: 400">
        Här kan ni läsa mer om tjänsterna som hjälpte <?php the_field('foretagsnamn'); ?> säkra driften av sin verksamhet
        </h3>
    </div>

    <!-- Loopa kategorier -->
    <div class="card-grid">
    <?php if(get_field('kategori')): ?>
        <?php while(has_sub_field('kategori')): ?>
        <a href="<?php the_sub_field('lank'); ?>" class="card zoom">
            <div class="card-content" style="background-image: url('<?php the_sub_field('bild'); ?>')">
                <p><?php the_sub_field('kategorinamn'); ?></p>
                <p><?php the_sub_field('underrubrik'); ?></p>

                <span href="<?php the_sub_field('lank'); ?>" class="btn-primary">Läs mer »</span>
            </div>
        </a>
        <?php endwhile; ?>
    <!-- //Loop -->
    <?php endif; ?>
</div>
<?php get_footer(); ?>