<?php
/**
 * Template Name: Nyheter (Samtliga)
 *
**/
get_header(); ?>

<!-- WP Content -->
<div id="entry" class="content">
    <h1>Nyheter</h1>

    <div class="page-template">

    <div class="posts loop">

        <?php
            $args = array(
                'post_type' => 'post',
                'category_name' => 'Nyheter'
            );

            $post_query = new WP_Query($args);
            if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                    $post_query->the_post();
                ?>

                <div class="post_item">
                    <div class="col">
                        <p class="title"><?php the_title(); ?></p>
                        <span class="date"><?php the_date(); ?></span>

                        <div class="excerpt">
                            <?php
                                if (has_excerpt()) {
                                    echo wp_trim_words( get_the_excerpt(), 60 );
                                } else {
                                    echo wp_trim_words( get_the_content(), 60 );
                                }
                            ?>
                        </div>

                        <a href="<?php the_permalink(); ?>" class="btn-primary">Läs mer »</a>
                    </div>
                    <div class="col">
                        <?php $img_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );  ?>
                        <div class="thumb" style="background-image:url(<?php echo $img_url ?>);"></div>
                    </div>
            </div>
            <?php
            }
        }
        ?>
    <!--//Loop end-->
    </div>
</div>

<?php get_footer(); ?>