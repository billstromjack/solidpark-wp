<?php
/**
 * Template Name: Mallen för Kundcase-sidan
 *
**/
get_header(); 

function get_customer_cases_pages() {
    $args = [
        'post_type' => 'page',
        'nopaging' => true,
        'meta_key' => '_wp_page_template',
        'meta_value' => 'template-customercase.php'
    ];
    $pages = get_posts( $args );

    return $pages;
} ?>

<!-- WP Content -->
<div id="entry" class="content page">
        <div class="page-template">
            <h1><?php the_title(); ?></h1>

            <div class="row">
                <p class="preamble">
                    <?php the_field('underrubrik'); ?>
                </p>
            </div>
        </div>
    </div>
<!--# WP Content -->

<div class="card-grid">
    <?php
    //echo json_encode(get_customer_cases_pages());
    foreach(get_customer_cases_pages() as $page) {
        $url = wp_get_attachment_url( get_post_thumbnail_id($page->ID), 'thumbnail' ); ?>
        <a href="<?php echo get_post_permalink($page->ID);?>" class="card zoom">
            <div class="card-content" style="background-image: url('<?php echo $url; ?>')">
                <?php
                    // if ( get_field('kortrubriksfarg', $page->ID)) {
                    //     $color = '<p color="' . the_field('kortrubriksfarg', $page->ID) . '">';
                    // }
                    if ( get_field('kortrubrik', $page->ID) ) {
                        ?>
                        <p><?php the_field('kortrubrik', $page->ID); ?></p>
                        <?php
                    } else { ?>
                        <p><?php echo $page->post_title; ?></p>
                    <?php
                    }

                    if ( get_field('kort_underrubrik', $page->ID) ) {
                        ?>
                        <span><?php the_field('kort_underrubrik', $page->ID); ?></span>
                        <?php
                    } else { ?>
                        <span></span>
                    <?php
                    }
                    

                ?>
                <p><?php echo substr($page->post_content, 0, 50); ?></p>

                <span class="btn-primary">Läs mer »</span>
            </div>
        </a> <?php
    }
    ?>
</div>

<?php get_footer(); ?>

<!--   rubrik, underrubrik,  -->