<?php
/**
 * Template Name: Kategorier
 */
function curr_cat()
{
    $requestUri = $_SERVER['REQUEST_URI'];

   # Remove query string
    $requestUri = trim(strstr($requestUri, '?', true), '/');
   # Note that delimeter is '/'
    $arr = explode('/', $requestUri);
    $count = count($arr);
    return $arr[$count - 1];
}

$cat = null;
if (isset($_GET['c'])) {
    $cat = $_GET['c'];
  } else {
    //Handle the case where there is no parameter
  }
 
get_header(); ?>
<!-- WP Content -->
<div id="entry" class="content">
    <h1>
        <?php echo $cat; ?>
    </h1>

    <div class="posts loop">

        <?php
            $args = array(
                'post_type' => 'post',
                'category_name' => $cat
            );

            $post_query = new WP_Query($args);
            if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                    $post_query->the_post();
                ?>
                <?php get_template_part( 'entry' ); ?>

        <?php
            }
        }
        ?>
        <!--//Loop end-->
    </div>
</div>

<?php get_footer(); ?>