<div class="post_item">

    <div class="col">
        <p class="title">
            <?php the_title(); ?>
        </p>

        <!-- //TODO: if katerogi = bloggartikel så visa datum --> 
        <span class="date">
            <?php echo $post->date; ?>
        </span>

        <div class="excerpt">
            <?php
                if (has_excerpt()) {
                    echo wp_trim_words( get_the_excerpt(), 60 );
                } else {
                    echo wp_trim_words( get_the_content(), 60 );
                }
            ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="btn-primary">Läs mer »</a>
    </div>

    <div class="col">
        <?php $img_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );  ?>
        <div class="thumb" style="background-image:url(<?php echo $img_url ?>);"></div>
    </div>

</div>