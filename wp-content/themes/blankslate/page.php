<?php

$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
$thumb_url = $thumb_url_array[0];

get_header(); ?>

<div id="entry" class="content page">
        <div class="page-template">
            <?php echo do_shortcode("[breadcrumb]"); ?>
            <h1><?php the_title(); ?></h1>

            <div class="row">

                <div class="col">
                    <p class="preamble">

                        <?php the_field('ingress'); ?>

                    </p>

                    <?php the_field('innehall'); ?>

                </div>

                <div class="col">

                    <div class="featured_img-container">
                        <img src="<?php echo $thumb_url ?>" alt="<?php echo $thumb_alt; ?>">
                    </div>

                    <?php if(get_field('hogerstallt_kort')): ?>
                        <?php while(has_sub_field('hogerstallt_kort')): ?>
                        <div class="page-card">
                                <h2><?php the_sub_field('rubrik'); ?></h2>
                                <?php the_sub_field('innehall'); ?>
                        </div>
                        <?php endwhile; ?>
                    <!-- //Loop -->
                    <?php endif; ?>


                </div>
            </div>
        </div>
    </div>
    <!--# WP Content -->

    <div class="section centered branded">
        <h3 style="font-weight: 400">
        Här kan ni läsa mer om tjänsterna som hjälpte <?php the_field('foretagsnamn'); ?> säkra driften av sin verksamhet
        </h3>
    </div>
</div>
<?php get_footer(); ?>